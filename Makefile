# https://makefiletutorial.com/
CC=g++
NAME=snake
SRC_DIR=./src
BUILD_DIR=./build


compile:
	@mkdir -p $(BUILD_DIR)
	$(CC) -o $(BUILD_DIR)/$(NAME) $(SRC_DIR)/*.cpp

install:
	@cp $(BUILD_DIR)/$(NAME) ~/$(NAME)

clean:
	@rm -rf $(BUILD_DIR)

purge: clean
	@rm ~/$(NAME)

