#ifndef VECTOR_H
#define VECTOR_H
#include "moves_enum.h"

class Vector
{
public:
  int x, y;
  static bool isSame(Vector *v1, Vector *v2);
  static Vector add(Vector *v1, Vector *v2);
  static Vector diff(Vector *v1, Vector *v2);
  void setVector(int x, int y);
  void setVector(Vector *v);
  Vector(int x, int y);
  Vector(Vector *v);
  Vector(Moves direction);
  Vector();
  ~Vector();
};
#endif