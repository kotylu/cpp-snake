#ifndef NODE_H
#define NODE_H

template<class T>
class Node {
  public:;
    T value;
    Node *next;
    Node(T value, Node *next);
    Node(T value);
};

#endif
