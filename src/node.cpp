#ifndef NODE_CPP
#define NODE_CPP
#include "node.h"

template<class T>
Node<T>::Node(T value, Node<T> *next)
  : value { value }, next { next } {

}

template<class T>
Node<T>::Node(T value)
  : value { value } {
    this->next = nullptr;
}
#endif