#ifndef LINKED_LIST_CPP
#define LINKED_LIST_CPP
#include "node.h"
#include "node.cpp"
#include "linked_list.h"
#include <stdlib.h>
#include <iostream>

template<class T>
LinkedList<T>::LinkedList() {
  this->root = nullptr;
  this->length = 0;
}

template<class T>
LinkedList<T>::~LinkedList() {
  if (this->root) {
    Node<T> *head = this->root;
    while (head) {
      Node<T> *curr = head;
      head = curr->next;
      delete curr;
    }
  }
}

template<class T>
void LinkedList<T>::add(T value) {
  this->length++;
  Node<T> *newNode = new Node<T>(value);
  if (!this->root) {
    this->root = newNode;
    return;
  }
  Node<T> *last;
  for (Node<T> *ptr = this->root; ptr; ptr = ptr->next) {
    last = ptr;
  }
  last->next = newNode;
}

template<class T>
T LinkedList<T>::getItem(int index) {
  Node<T> *prev = this->root;
  for (int i = 0; i < index; i++) {
    prev = prev->next;
  }
  return prev->value;
}

template<class T>
void LinkedList<T>::setItem(int index, T value) {
  Node<T> *item = this->root;
  for (int i = 0; i < index; i++) {
    item = item->next;
  }
  //std::cout << "old value: " << item->value << std::endl;
  item->value = value;
  //std::cout << "new value: " << item->value << std::endl;
}

template<class T>
Node<T> * LinkedList<T>::getRoot() {
  return this->root;
}

template<class T>
size_t LinkedList<T>::getSize() {
  return this->length;
}

#endif