#include <unistd.h> // sleep()
#include "snake.h"
#include "vector.h"
#include "moves_enum.h"
#include "linked_list.h"
#include "linked_list.cpp"
#include "node.h"
#include "node.cpp"
#include <iostream>
#include <stdlib.h>

void Snake::moveAndExtend(Moves direction) {
  this->extendOnNextMove = true;
  Vector newBodyPart = this->body.getItem(this->body.getSize()-1);
  this->body.add(newBodyPart);
  this->move(direction);
}

void Snake::move(Moves direction) {
  Vector prevPos;
  for (size_t i = 0; i < this->body.getSize(); i++) {
    if (i == 0) { // HEAD
      std::cout << "HEAD" << std::endl;
      Vector currPos = this->body.getItem(0);
      Vector bodyPartDirection {direction};
      Vector newPos = Vector::add(&currPos, &bodyPartDirection);
      std::cout << "HEAD DIR: (" << bodyPartDirection.x << ", " << bodyPartDirection.y << ")" << std::endl;
      std::cout << "old pos: (" << currPos.x << ", " << currPos.y << ")" << std::endl;
      std::cout << "new pos: (" << newPos.x << ", " << newPos.y << ")" << std::endl;
      this->body.setItem(0, newPos);
      prevPos = currPos;
    }
    else if (i == this->body.getSize() - 1 && this->extendOnNextMove) { // TAIL
      std::cout << "TAIL" << std::endl;
      this->extendOnNextMove = false;
      return;
    }
    else { // BODY
      std::cout << "BODY" << std::endl;
      Vector currPos = this->body.getItem(i);
      Vector bodyPartDirection = Vector::diff(&prevPos, &currPos);
      Vector newPos = Vector::add(&currPos, &bodyPartDirection);
      std::cout << "BODY DIR: (" << bodyPartDirection.x << ", " << bodyPartDirection.y << ")" << std::endl;
      std::cout << "old pos: (" << currPos.x << ", " << currPos.y << ")" << std::endl;
      std::cout << "new pos: (" << newPos.x << ", " << newPos.y << ")" << std::endl;
      this->body.setItem(i, newPos);
      prevPos = currPos;
    }
  }
}

bool Snake::isBodyOnPosition(int x, int y) {
  Vector v(x, y);
  for (Node<Vector> *ptr = this->body.getRoot(); ptr; ptr = ptr->next) {
    if (Vector::isSame(&v, &ptr->value)) {
      return true;
    }
  }
  return false;
}

bool Snake::hasHeadCrossedBody() {
  Vector headPos = this->body.getItem(0);
  for (Node<Vector> *ptr = this->body.getRoot()->next; ptr; ptr = ptr->next) {
    if (Vector::isSame(&headPos, &ptr->value)) {
      return true;
    }
  }
  return false;
}

Snake::Snake(Vector *startPos) {
  this->extendOnNextMove = false;
  this->body.add(Vector(startPos));
}
