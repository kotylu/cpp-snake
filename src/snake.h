#ifndef SNAKE_H
#define SNAKE_H
#include "vector.h"
#include "moves_enum.h"
#include "vector.h"
#include "linked_list.h"
#include "linked_list.cpp"

class Snake
{
  private:
    bool extendOnNextMove;
  public:
    LinkedList<Vector> body;
    Snake(Vector *startPos);
    void move(Moves direction);
    void moveAndExtend(Moves direction);
    bool isBodyOnPosition(int x, int y);
    bool hasHeadCrossedBody();
};
#endif
