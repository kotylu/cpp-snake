#ifndef LINKED_LIST_H
#define LINKED_LIST_H
#include "node.h"
#include <stdlib.h>

template<class T>
class LinkedList {
  private:
    Node<T> *root;
    size_t length;
  public:
    LinkedList();
    ~LinkedList();
    void add(T value);
    Node<T> *getRoot();
    T getItem(int index);
    void setItem(int index, T value);
    size_t getSize();
};
#endif
