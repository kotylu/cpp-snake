#include "vector.h"

Vector::Vector(int x, int y)
{
  this->x = x;
  this->y = y;
}

Vector::Vector() 
{
  this->x = 0;
  this->y = 0;
}

Vector::Vector(Vector *v) {
  this->x = v->x;
  this->y = v->y;
}

Vector::Vector(Moves direction) {
  switch (direction)
  {
    case UP:
      this->setVector(0, -1);
      break;
    case DOWN:
      this->setVector(0, 1);
      break;
    case RIGHT:
      this->setVector(1, 0);
      break;
    case LEFT:
      this->setVector(-1, 0);
      break;
  }
}

Vector Vector::add(Vector *v1, Vector *v2) {
  Vector result(v1->x + v2->x, v1->y + v2->y);
  return result;
}

Vector Vector::diff(Vector *v1, Vector *v2) {
  Vector result(v1->x - v2->x, v1->y - v2->y);
  return result;
}

Vector::~Vector()
{
  this->x = 0;
  this->y = 0;
}

bool Vector::isSame(Vector *v1, Vector *v2) {
  return (v1->x == v2->x) && (v1->y == v2->y);
}

void Vector::setVector(int x, int y)
{
  this->x = x;
  this->y = y;
}

void Vector::setVector(Vector *v)
{
  this->x = v->x;
  this->y = v->y;
}