#include "linked_list.h"
#include "linked_list.cpp"
#include "node.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include "vector.h"
#include <iostream>
#include <cstdio>
#include "moves_enum.h"
#include "snake.h"

using namespace std;

// -----------
struct termios oldSettings, newSettings;
// -----------

bool isGameOver;
int points;
Vector fruitPos;
const Vector SCREEN(40, 20);
Moves dir;
Snake *snake;

int getRandomInt(int min, int max) {
  srand(time(0));
  return (rand() % (max - min + 1)) + min;
}

void setFruitToRandomPosition() {
  int x = getRandomInt(1, SCREEN.x-1);
  int y = getRandomInt(1, SCREEN.y-1);
  fruitPos.setVector(x, y);
}

void setup() {
  isGameOver = false;
  points = 0;
  Vector snakePos;
  snakePos.setVector(SCREEN.x/2, SCREEN.y/2);
  snake = new Snake(&snakePos);
  setFruitToRandomPosition();
  //--------- key pressed init
  tcgetattr( fileno( stdin ), &oldSettings );
  newSettings = oldSettings;
  newSettings.c_lflag &= (~ICANON & ~ECHO);
  tcsetattr( fileno( stdin ), TCSANOW, &newSettings );
}

void draw() {
  system("clear");
  // game is over
  if (isGameOver) {
    for (int i = 0; i < SCREEN.y; i++) {
      cout << endl;
      if (i == SCREEN.y / 2) {
        for (int j = 0; j < SCREEN.x/2-4; j++) {
          cout << " ";
        }
        cout << "GAME OVER";
      }
    }
    return;
  }
  // game 
  cout << endl;
  for (int y = 0; y < SCREEN.y; y++) {
    for (int x = 0; x < SCREEN.x; x++) {
      if (y == 0 || y+1 == SCREEN.y) {
        cout << "#";
        continue;
      }
      if (x == 0 || x+1 == SCREEN.x) {
        cout << "#";
      }
      else if (snake->isBodyOnPosition(x, y)) {
        cout << "S";
      }
      else if (x == fruitPos.x && y == fruitPos.y) {
        cout << "X";
      }
      else {
        cout << " ";
      }
    }
    cout << endl;
  }
  cout << endl;
  cout << "POINTS: " << points << endl;
  cout << "SNAKE LENGTH: " << snake->body.getSize() << endl;
  cout << "SCREEN: " << SCREEN.x << ":" << SCREEN.y << endl;
  cout << "SNAKE: " << snake->body.getRoot()->value.x << ":" << snake->body.getRoot()->value.y << endl;
  cout << "FRUIT: " << fruitPos.x << ":" << fruitPos.y << endl;
}

void input() {
  // ----------- key pressed loop
  fd_set set;
  struct timeval tv;

  tv.tv_sec = 1;
  tv.tv_usec = 0;

  FD_ZERO( &set );
  FD_SET( fileno( stdin ), &set );

  int res = select( fileno( stdin )+1, &set, NULL, NULL, &tv );

  if ( res > 0 )
  {
      char c;
      read( fileno( stdin ), &c, 1 );
      switch (c)
      {
        case 'a':
          dir = LEFT;
          break;
        case 's':
          dir = DOWN;
          break;
        case 'd':
          dir = RIGHT;
          break;
        case 'w':
          dir = UP;
          break;
        default:
          dir = STOP;
      }
  }
}

void logic() {
  Vector head = snake->body.getItem(0);
  isGameOver = head.x == 0 || head.x == SCREEN.x-1 ||
    head.y == 0 || head.y == SCREEN.y-1 || snake->hasHeadCrossedBody();
  if (Vector::isSame(&head, &fruitPos)) {
    points++;
    setFruitToRandomPosition();
    snake->moveAndExtend(dir);
  }
  else {
    snake->move(dir);
  }
}

void end() {
  tcsetattr( fileno( stdin ), TCSANOW, &oldSettings );
  delete snake;
}

int main() {
  setup();
  while (!isGameOver) {
    draw();
    input();
    logic();
  }
  draw();
  end();
  /*
  LinkedList<int> ll;
  ll.add(0);
  ll.add(0);
  ll.add(0);
  ll.setItem(1, 1);
  ll.setItem(2, 2);
  cout << ll.getItem(0) << ll.getItem(1) << ll.getItem(2);

  cout << endl;

  LinkedList<Vector> xx;
  xx.add(Vector{1, 1});
  xx.add(Vector{2, 1});
  xx.add(Vector{3, 1});
  xx.add(Vector{4, 1});
  Vector newV {1, 1};
  Vector old = xx.getItem(0);
  Vector res = Vector::add(&newV, &old);
  xx.setItem(0, res);
  cout<<endl<<endl;
  for (Node<Vector> *ptr = xx.getRoot(); ptr; ptr = ptr->next) {
    cout << ptr->value.x << ptr->value.y << endl;
  }
  */
}
